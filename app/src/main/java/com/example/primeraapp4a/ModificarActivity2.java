package com.example.primeraapp4a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class ModificarActivity2 extends AppCompatActivity {

    EditText titulo, edicion;
    TextView mostrar;
    Button btnModificar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar2);
        titulo = findViewById(R.id.edit_titulo);
        edicion = findViewById(R.id.edit_edicion);
        mostrar = findViewById(R.id.text_mostrar);
        btnModificar = findViewById(R.id.button_modificar2);

        Bundle b = this.getIntent().getExtras();
        final int posicionEnSugar = b.getInt("Posición")+1;
        final long id = b.getLong("Id");
        mostrar.setText("Modificar campo #" + posicionEnSugar);
        titulo.setText(b.getString("Titulo"));
        edicion.setText(b.getString("Edición"));

        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!titulo.getText().toString().trim().isEmpty() && !edicion.getText().toString().trim().isEmpty()) {
                    Book libro_a_modificar = Book.findById(Book.class, id);
                    libro_a_modificar.setTitle(titulo.getText().toString());
                    libro_a_modificar.setEdition(edicion.getText().toString());
                    libro_a_modificar.save();
                    Toast.makeText(ModificarActivity2.this, "Se modificó correctamente el campo", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }
}
