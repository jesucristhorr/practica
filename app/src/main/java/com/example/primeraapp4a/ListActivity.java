package com.example.primeraapp4a;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    ListView listViewLibros;
    EditText titulo, edicion;
    Button btnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listViewLibros = findViewById(R.id.list_view_libros);

        List<Book> books = Book.listAll(Book.class);
        List<String> listaTextoLibros = new ArrayList<>();

        for (Book libro: books) {
            Log.e("Libro", libro.getTitle() + " " + libro.getEdition());
            listaTextoLibros.add(libro.getTitle() + " " + libro.getEdition());

        }

        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaTextoLibros);
        listViewLibros.setAdapter(itemsAdapter);
    }
}
