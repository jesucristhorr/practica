package com.example.primeraapp4a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.lang.reflect.Array;

public class ActividadRegistrar extends AppCompatActivity {

    Button enviarDatos;
    EditText nombres;
    EditText apellidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_registrar);
        enviarDatos = (Button) findViewById(R.id.buttonEnviar);
        nombres = (EditText) findViewById(R.id.texto_ingresar_nombre);
        apellidos = (EditText) findViewById(R.id.texto_ingresar_apellidos);
        enviarDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadRegistrar.this, RecibirDatosPersonalesActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("nombres", nombres.getText().toString());
                bundle.putString("apellidos", apellidos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
