package com.example.primeraapp4a;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        ArrayList<String> tareas = new ArrayList<>();

        for (int item = 0; item <= 10; item++) {
            if (item == 0) {
                tareas.add("Top 10 Superhéroes");
                continue;
            }
            tareas.add("Superhéroe " + item);
        }

        TareasRecyclerViewAdapter adapter = new TareasRecyclerViewAdapter(this, tareas);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
