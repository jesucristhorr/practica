package com.example.primeraapp4a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

public class LiteActivity extends AppCompatActivity {

    TextView textdb;
    EditText titulo, edicion;
    Button btnGuardar, btnEliminar, btnModificar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lite);
        titulo = findViewById(R.id.editTexttitulo);
        edicion = findViewById(R.id.editTextedicion);
        btnGuardar = findViewById(R.id.button_guardar);
        btnEliminar = findViewById(R.id.button_eliminar);
        btnModificar = findViewById(R.id.button_modificar);

//        textdb = findViewById(R.id.text_db);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!titulo.getText().toString().trim().isEmpty() || !edicion.getText().toString().trim().isEmpty()) {
                    Book book = new Book(titulo.getText().toString(), edicion.getText().toString());
                    book.save();
                }
            }
        });

        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LiteActivity.this, ModificarActivity.class);
                startActivity(intent);
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LiteActivity.this, ListaEliminarActivity.class);
                startActivity(intent);
            }
        });

    }
}
