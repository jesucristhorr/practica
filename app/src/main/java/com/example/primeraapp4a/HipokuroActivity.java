package com.example.primeraapp4a;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CalendarView;
import android.widget.TextView;

public class HipokuroActivity extends AppCompatActivity {

    CalendarView uwu;
    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hipokuro);
        uwu = findViewById(R.id.calendarView);
        texto = findViewById(R.id.textA);

        uwu.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                texto.setText(i + "/" + i1 + "/" + i2);
            }
        });
    }
}
