package com.example.primeraapp4a;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RecibirParametroActivity extends AppCompatActivity {

    TextView textoFrontal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);
        textoFrontal = (TextView) findViewById(R.id.lblParametro);
        Bundle bundle = this.getIntent().getExtras();
        double nroMostrar = bundle.getDouble("dato");
        nroMostrar = nroMostrar*1.119;
        BigDecimal bd = new BigDecimal(nroMostrar).setScale(2, RoundingMode.HALF_EVEN);
        textoFrontal.setText("$" + String.valueOf(bd));
    }
}
