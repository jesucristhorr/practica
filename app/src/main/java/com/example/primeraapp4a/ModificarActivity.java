package com.example.primeraapp4a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ModificarActivity extends AppCompatActivity {

    ListView lista_para_modificar;
    List<Book> libros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar);
        lista_para_modificar = findViewById(R.id.list_modificar);

        libros = Book.listAll(Book.class);
        List<String> lista_con_strings = new ArrayList<>();

        for (Book books:libros) {
            lista_con_strings.add(books.getTitle() + "\n" + books.getEdition());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, lista_con_strings);
        lista_para_modificar.setAdapter(arrayAdapter);

        lista_para_modificar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ModificarActivity.this, ModificarActivity2.class);
                Bundle b = new Bundle();
                Book libro_escogido = libros.get(i);
                b.putLong("Id", libro_escogido.getId());
                b.putString("Titulo", libro_escogido.getTitle());
                b.putString("Edición", libro_escogido.getEdition());
                b.putInt("Posición", i);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        libros = Book.listAll(Book.class);
        List<String> lista_con_strings = new ArrayList<>();

        for (Book books:libros) {
            lista_con_strings.add(books.getTitle() + "\n" + books.getEdition());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, lista_con_strings);
        lista_para_modificar.setAdapter(arrayAdapter);
    }
}
