package com.example.primeraapp4a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PasarParametroActivity extends AppCompatActivity {

    EditText cajaDatos;
    Button botonEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);
        cajaDatos = (EditText) findViewById(R.id.text_input_frase);
        botonEnviar = (Button) findViewById(R.id.button_pasar_parametro);
        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PasarParametroActivity.this, RecibirParametroActivity.class);
                // se crea un objeto tipo Bundle que será la vitácora de parámetros a enviar
                Bundle bundle = new Bundle();
                // el método put fija los parámetros a enviar mediante un id
                bundle.putDouble("datoc", Double.parseDouble(cajaDatos.getText().toString()));
                bundle.putDouble("dato", Double.parseDouble(cajaDatos.getText().toString()));
                // el método putExtras envía un objeto tipo bundle como un sólo parámetro entre actividades
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
