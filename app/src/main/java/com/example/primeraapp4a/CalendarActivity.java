package com.example.primeraapp4a;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CalendarView;
import android.widget.TextView;

public class CalendarActivity extends AppCompatActivity {

    CalendarView calendario;
    TextView textoCalendario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        calendario = findViewById(R.id.calendar);
        textoCalendario = findViewById(R.id.textCalendar);

        calendario.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                textoCalendario.setText(String.valueOf(i) + "/" + String.valueOf(i1+1) + "/" +String.valueOf(i2));
            }
        });
    }
}
