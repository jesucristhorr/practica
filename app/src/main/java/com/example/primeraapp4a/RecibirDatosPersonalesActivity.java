package com.example.primeraapp4a;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class RecibirDatosPersonalesActivity extends AppCompatActivity {

    TextView nombres;
    TextView apellidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_datos_personales);
        nombres = (TextView) findViewById(R.id.texto_presentar_nombres);
        apellidos = (TextView) findViewById(R.id.texto_presentar_apellidos);
        Bundle bundle = this.getIntent().getExtras();
        nombres.setText("Los nombres del cliente son: " + bundle.getString("nombres"));
        apellidos.setText("Los apellidos del cliente son: " + bundle.getString("apellidos"));

    }
}
