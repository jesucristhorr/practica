package com.example.primeraapp4a;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class TareasRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> items;
    Typeface fontBold;
    Context mContext;
    private final int ITEM = 0, TITLE = 1;

    public TareasRecyclerViewAdapter(Context context, ArrayList<String> items) {
        this.mContext = context;
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TITLE;
        } else {
            return ITEM;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (i) {
            case ITEM:
                View v1 = inflater.inflate(R.layout.recycler_item, viewGroup, false);
                viewHolder = new ViewHolderItem(v1);
                break;
            case TITLE:
                View v2 = inflater.inflate(R.layout.recycler_title, viewGroup, false);
                viewHolder = new ViewHolderTitulo(v2);
                break;
            default:
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        switch (viewHolder.getItemViewType()) {
            case ITEM:
                ViewHolderItem vh1 = (ViewHolderItem) viewHolder;
                configureViewHolderTitulo(vh1, i);
                break;
            case TITLE:
                ViewHolderTitulo vh2 = (ViewHolderTitulo) viewHolder;
                configureViewHolderItem(vh2, i);
                break;
        }
    }

    private void configureViewHolderTitulo(ViewHolderItem vh1, int position) {
        String task = items.get(position);
        if (task != null) {
            switch (task) {
                case "Superhéroe 1":
                    vh1.getTaskName().setText("Spiderman");
                    vh1.getTextStatus().setText(task);
                    vh1.getImageViewNombre().setImageResource(R.drawable.spiderman);
                    break;
                case "Superhéroe 2":
                    vh1.getTaskName().setText("Flash");
                    vh1.getTextStatus().setText(task);
                    vh1.getImageViewNombre().setImageResource(R.drawable.flash);
                    break;
                case "Superhéroe 3":
                    vh1.getTaskName().setText("Batman");
                    vh1.getTextStatus().setText(task);
                    vh1.getImageViewNombre().setImageResource(R.drawable.batman);
                    break;
                case "Superhéroe 4":
                    vh1.getTaskName().setText("Ironman");
                    vh1.getTextStatus().setText(task);
                    vh1.getImageViewNombre().setImageResource(R.drawable.ironman);
                    break;
                case "Superhéroe 5":
                    vh1.getTaskName().setText("Capitán América");
                    vh1.getTextStatus().setText(task);
                    vh1.getImageViewNombre().setImageResource(R.drawable.capitan);
                    break;
                case "Superhéroe 6":
                    vh1.getTaskName().setText("Superman");
                    vh1.getTextStatus().setText(task);
                    vh1.getImageViewNombre().setImageResource(R.drawable.superman);
                    break;
                case "Superhéroe 7":
                    vh1.getTaskName().setText("Hulk");
                    vh1.getTextStatus().setText(task);
                    vh1.getImageViewNombre().setImageResource(R.drawable.hulk);
                    break;
                case "Superhéroe 8":
                    vh1.getTaskName().setText("Antman");
                    vh1.getTextStatus().setText(task);
                    vh1.getImageViewNombre().setImageResource(R.drawable.antman);
                    break;
                case "Superhéroe 9":
                    vh1.getTaskName().setText("Wonder woman");
                    vh1.getTextStatus().setText(task);
                    vh1.getImageViewNombre().setImageResource(R.drawable.wonder);
                    break;
                case "Superhéroe 10":
                    vh1.getTaskName().setText("Linterna verde");
                    vh1.getTextStatus().setText(task);
                    vh1.getImageViewNombre().setImageResource(R.drawable.green);
                    break;
            }
        }
    }

    private void configureViewHolderItem(ViewHolderTitulo vh2, int position) {
        String separatorString = items.get(position);
        vh2.getTextViewSeparator().setText(separatorString);
        vh2.getTextViewSeparator().setTypeface(fontBold);
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder implements
            View.OnClickListener {
        private TextView TextViewNombreTarea, TextViewEstadoTarea;
        private ImageView ImageViewNombre;
        public ViewHolderItem(View v) {
            super(v);
            TextViewNombreTarea = (TextView) v.findViewById(R.id.taskName);
            TextViewEstadoTarea = (TextView) v.findViewById(R.id.taskStatus);
            ImageViewNombre = (ImageView) v.findViewById(R.id.imageRecycler);
            v.setOnClickListener(this);
        }
        public ImageView getImageViewNombre() {
            return ImageViewNombre;
        }
        public void setImageViewNombre(ImageView image) {
            this.ImageViewNombre = image;
        }
        public TextView getTaskName() {
            return TextViewNombreTarea;
        }
        public void setTaskName(TextView label1) {
            this.TextViewNombreTarea = label1;
        }
        public TextView getTextStatus() {
            return TextViewEstadoTarea;
        }
        public void setTextStatus(TextView textView) {
            this.TextViewEstadoTarea = textView;
        }
        @Override
        public void onClick(View view) {
            // obtiene la posición del item
            int position = getLayoutPosition();
        }
    }

    public class ViewHolderTitulo extends RecyclerView.ViewHolder {
        private TextView TextViewTitulo;
        public ViewHolderTitulo(View v) {
            super(v);
            TextViewTitulo = v.findViewById(R.id.text_separator);
        }
        public TextView getTextViewSeparator() {
            return TextViewTitulo;
        }
        public void setTextViewSeparator(TextView separator) {
            this.TextViewTitulo = separator;
        }
    }
}