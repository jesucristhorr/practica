package com.example.primeraapp4a;

import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.orm.SugarRecord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListaEliminarActivity extends AppCompatActivity {

    ListView lista_para_eliminar;
    List<Book> bookList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_eliminar);

        lista_para_eliminar = findViewById(R.id.lista_eliminar);

        bookList = Book.listAll(Book.class);
        List<String> strings = new ArrayList<>();

        for (Book libros:bookList) {
            strings.add(libros.getTitle() + "\n" + libros.getEdition());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, strings);
        lista_para_eliminar.setAdapter(arrayAdapter);

        lista_para_eliminar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, int position, long l) {
                final int libro_elegido = position;
                int campo = position+1;
                DialogInterface.OnClickListener dialogClickInterface = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case DialogInterface.BUTTON_POSITIVE:
                                Book book = bookList.get(libro_elegido);
                                book.delete();
                                Snackbar.make(view, "Se eliminó el campo correctamente", Snackbar.LENGTH_SHORT).show();
//                                Toast.makeText(ListaEliminarActivity.this, "Se eliminó el campo correctamente", Toast.LENGTH_SHORT).show();
                                refrescarLista();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle("Eliminar campo")
                        .setMessage("¿Desea eliminar el campo #" + campo + "?")
                        .setPositiveButton("Sí", dialogClickInterface)
                        .setNegativeButton("No", dialogClickInterface)
                        .show();
            }
        });
    }

    private void refrescarLista() {
        bookList = Book.listAll(Book.class);
        List<String> strings = new ArrayList<>();

        for (Book libros:bookList) {
            strings.add(libros.getTitle() + "\n" + libros.getEdition());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, strings);
        lista_para_eliminar.setAdapter(arrayAdapter);
    }
}
