package com.example.primeraapp4a;

import android.Manifest;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    public Button buttonLogin, buttonGuardar, buttonBuscar, buttonParametro, buttonRecycler, buttonCamara, buttonCalendar, buttonMInterna, buttonSQLite, buttonComplementario, buttonLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        buttonLogin = (Button) findViewById(R.id.buttonLogin);
//        buttonLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // se genera la navegabilidad entre la actividad principal y la actividad de login
//                Intent intent = new Intent(MainActivity.this, ActividadLogin.class);
//                startActivity(intent);
//            }
//        });
//        buttonGuardar =  (Button) findViewById(R.id.buttonGuardar);
//        buttonGuardar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // se genera la navegabilidad entre la actividad principal y la actividad de login
//                Intent intent = new Intent(MainActivity.this, ActividadRegistrar.class);
//                startActivity(intent);
//            }
//        });
//        buttonBuscar = (Button) findViewById(R.id.buttonBuscar);
//        buttonBuscar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // se genera la navegabilidad entre la actividad principal y la actividad de login
//                Intent intent = new Intent(MainActivity.this, ActividadBuscar.class);
//                startActivity(intent);
//            }
//        });
//        buttonParametro = (Button) findViewById(R.id.buttonPasarParametro);
//        buttonParametro.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, PasarParametroActivity.class);
//                startActivity(intent);
//            }
//        });
//        buttonRecycler = findViewById(R.id.buttonRecyclerView);
//        buttonRecycler.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, RecyclerActivity.class);
//                startActivity(intent);
//            }
//        });
//        buttonCamara = findViewById(R.id.button_camara);
//        buttonCamara.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, CamaraActivity.class);
//                startActivity(intent);
//            }
//        });
//        buttonCalendar = findViewById(R.id.button_calendar);
//        buttonCalendar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, CalendarActivity.class);
//                startActivity(intent);
//            }
//        });
        buttonMInterna = findViewById(R.id.button_memoria_interna);
        buttonMInterna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MemoriaInternaActivity.class);
                startActivity(intent);
            }
        });
//        buttonSQLite = findViewById(R.id.button_lite);
//        buttonSQLite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, LiteActivity.class);
//                startActivity(intent);
//            }
//        });
        buttonComplementario = findViewById(R.id.button_complementario);
        buttonComplementario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ComplementarioActivity.class);
                startActivity(intent);
            }
        });
//        buttonLista = findViewById(R.id.button_list_view);
//        buttonLista.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, ListActivity.class);
//                startActivity(intent);
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcion_actividad_registrar:
                intent = new Intent(MainActivity.this, ActividadRegistrar.class);
                startActivity(intent);
                break;
            case R.id.opcion_actividad_buscar:
                intent = new Intent(MainActivity.this, ActividadBuscar.class);
                startActivity(intent);
                break;
            case R.id.opcion_actividad_login:
                intent = new Intent(MainActivity.this, ActividadLogin.class);
                startActivity(intent);
                break;
        }
        return true;
    }
}
